from django_filters.rest_framework import DjangoFilterBackend
from drf_yasg.utils import swagger_auto_schema
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.permissions import AllowAny, IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from django.core.exceptions import ObjectDoesNotExist
from rest_framework_simplejwt.views import TokenObtainPairView

from .utils import recalculation_basket, PaginationProducts
from .serializer import (
    CategorySerializer,
    SubcategorySerializer,
    SubcategoryCreateSerializer,
    ProductListSerializer,
    ProductDetailSerializer,
    ProductCreateSerializer,
    BasketSerializer,
    AddToBasketSerializer,
    ChangeQTYBasketProductSerializer,
    OrderCreateSerializer,
    OrderSerializer,
    BasketProductSerializer,
    OrderStatusChangingSerializer,
)
from .models import Category, Subcategory, Product, Basket, BasketProduct, Order
from users.models import CustomUser
from .mixins import ViewSetActionPermissionMixin


class CategoriesView(ViewSetActionPermissionMixin, viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

    permission_action_classes = {
        'list': [AllowAny],
        'retrieve': [AllowAny],
        'create': [IsAdminUser],
        'update': [IsAdminUser],
        'destroy': [IsAdminUser],
        'partial_update': [IsAdminUser]
    }

    @swagger_auto_schema(operation_summary="Все категорий")
    def list(self, request, *args, **kwargs):
        """ # Получение всех категорий"""
        return super(CategoriesView, self).list(request)

    @swagger_auto_schema(operation_summary="Одна категория")
    def retrieve(self, request, *args, **kwargs):
        """ # Получение конкретной категории """
        return super(CategoriesView, self).retrieve(request)

    @swagger_auto_schema(operation_summary="Создать категорию")
    def create(self, request, *args, **kwargs):
        """ # Создание новой категории"""
        return super(CategoriesView, self).create(request)

    @swagger_auto_schema(operation_summary="Обноваить данные категории")
    @action(detail=True, methods=['patch'])
    def partial_update(self, request, *args, **kwargs):
        """ # Обновление данных категории"""
        return super(CategoriesView, self).partial_update(request)

    @swagger_auto_schema(operation_summary="Удалить категорию")
    def destroy(self, request, *args, **kwargs):
        """ # Удаление категории"""
        return super(CategoriesView, self).destroy(request)


class SubcategoriesView(ViewSetActionPermissionMixin, viewsets.ModelViewSet):
    queryset = Subcategory.objects.all()
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['category']
    permission_action_classes = {
        'list': [AllowAny],
        'retrieve': [AllowAny],
        'create': [IsAdminUser],
        'update': [IsAdminUser],
        'destroy': [IsAdminUser],
        'partial_update': [IsAdminUser]
    }

    def get_serializer_class(self):
        if self.action == 'create':
            return SubcategoryCreateSerializer
        return SubcategorySerializer

    @swagger_auto_schema(operation_summary="Все подкатегорий")
    def list(self, request, *args, **kwargs):
        """ # Получение всех подкатегорий"""
        return super(SubcategoriesView, self).list(request)

    @swagger_auto_schema(operation_summary="Одна подкатегории")
    def retrieve(self, request, *args, **kwargs):
        """ # Получение конкретной подкатегории"""
        return super(SubcategoriesView, self).retrieve(request)

    @swagger_auto_schema(operation_summary="Создать подкатегорю")
    def create(self, request, *args, **kwargs):
        """ # Создание новой подкатегории"""
        return super(SubcategoriesView, self).create(request)

    @swagger_auto_schema(operation_summary="Удалить подкатегорию")
    def destroy(self, request, *args, **kwargs):
        """ # Удаление подкатегории"""
        return super(SubcategoriesView, self).destroy(request)

    @swagger_auto_schema(operation_summary="Обновить данные подкатегории")
    def partial_update(self, request, *args, **kwargs):
        """ # Обновить данные подкатегории"""
        return super(SubcategoriesView, self).partial_update(request)


class ProductView(ViewSetActionPermissionMixin, viewsets.ModelViewSet):
    queryset = Product.objects.all()
    filter_backends = [SearchFilter, OrderingFilter]
    search_fields = ['name', 'subcategory__name']
    ordering_fields = ['price', 'name']
    pagination_class = PaginationProducts
    ordering = ['id']
    permission_action_classes = {
        'list': [AllowAny],
        'retrieve': [AllowAny],
        'create': [IsAdminUser],
        'update': [IsAdminUser],
        'destroy': [IsAdminUser],
        'partial_update': [IsAdminUser]
    }

    def get_serializer_class(self):
        if self.action == 'list':
            return ProductListSerializer
        if self.action == 'retrieve':
            return ProductDetailSerializer
        else:
            return ProductCreateSerializer

    @swagger_auto_schema(operation_summary="Все продукты")
    def list(self, request, *args, **kwargs):
        """ # Получение всех продуктов"""
        return super(ProductView, self).list(request)

    @swagger_auto_schema(operation_summary="Один продукт")
    def retrieve(self, request, *args, **kwargs):
        """ # Получение конкретного продукта"""
        return super(ProductView, self).retrieve(request)

    @swagger_auto_schema(operation_summary="Создать продукт")
    def create(self, request, *args, **kwargs):
        """ # Создание нового продукта"""
        return super(ProductView, self).create(request)

    @swagger_auto_schema(operation_summary="Удалить продукт")
    def destroy(self, request, *args, **kwargs):
        """ # Удаление продукта"""
        return super(ProductView, self).destroy(request)

    @swagger_auto_schema(operation_summary="Обновить данные продукта")
    def partial_update(self, request, *args, **kwargs):
        """ # Обновить данные продукта"""
        return super(ProductView, self).partial_update(request)


class BasketView(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    serializer_class = BasketSerializer

    @swagger_auto_schema(operation_summary="Одина корзина")
    @action(detail=True)
    def retrieve(self, request, *args, **kwargs):
        """Получение конкретной корзины"""
        customer = CustomUser.objects.filter(phone=request.user.phone).first()
        basket = Basket.objects.filter(owner=customer, in_order=False).first()
        serializer = BasketSerializer(basket)
        return Response(serializer.data)


class AddToBasketView(viewsets.ModelViewSet):
    serializer_class = AddToBasketSerializer
    permission_classes = [IsAuthenticated, ]

    @swagger_auto_schema(operation_summary="Добавить продукт в корзину")
    @action(detail=True, methods=['post'])
    def add_to_basket(self, request, *args, **kwargs):
        """ # Добавление продукта в корзину"""
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            customer = CustomUser.objects.filter(phone=request.user.phone).first()
            basket = Basket.objects.filter(owner=customer, in_order=False).first()
            if not basket:
                basket = Basket.objects.create(owner=customer)

            try:
                product = Product.objects.get(id=request.data.get('id'))
            except ObjectDoesNotExist:
                return Response({"error": f"Объект с id={request.data.get('id')} не найдет!"},
                                status=status.HTTP_400_BAD_REQUEST)

            basket_product, created = BasketProduct.objects.get_or_create(
                user=basket.owner, basket=basket, product=product
            )
            if created:
                basket.products.add(basket_product)
                recalculation_basket(basket)
                return Response({
                    'message': "Продукт успешно добавленн в корзину!",
                    'basket_product': BasketProductSerializer(basket_product,
                                                              context=self.get_serializer_context()).data
                }, status=status.HTTP_201_CREATED)
            return Response({'message': "Продукт уже есть в корзине!"}, status=status.HTTP_200_OK)


class ChangeProductQTYView(viewsets.ModelViewSet):
    serializer_class = ChangeQTYBasketProductSerializer
    permission_classes = [IsAuthenticated, ]

    @swagger_auto_schema(operation_summary="Изменить количесво продукта в корзине")
    @action(detail=True, methods=['post'])
    def change_qty(self, request, *args, **kwargs):
        """ # Изменение количества продуктов в корзине
        ---
        id - уникальный номер продукта в корзине
        action - операция производимаяя с количеством (addition - увуличение, subtraction - уменьшение)
        """
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):

            customer = CustomUser.objects.filter(phone=request.user.phone).first()
            basket = Basket.objects.filter(owner=customer, in_order=False).first()

            try:
                basket_product = BasketProduct.objects.get(id=request.data.get('id'), user=basket.owner, basket=basket)
            except ObjectDoesNotExist:
                return Response({"error": f"Объект с id={request.data.get('id')} не найдет!"},
                                status=status.HTTP_400_BAD_REQUEST)
            if request.data.get('action') == "addition":
                basket_product.quantity += 1
            elif request.data.get('action') == "subtraction":
                if basket_product.quantity == 1:
                    basket.products.remove(basket_product)
                basket_product.quantity -= 1
            else:
                return Response({"error": f"Неверное значение в поле action. Ожидалось 'addition' или 'subtraction'"},
                                status=status.HTTP_400_BAD_REQUEST)

            basket_product.save()
            recalculation_basket(basket)
            return Response({'message': "Количество продукта было изменено!"}, status=status.HTTP_200_OK)


class DeleteFromBasketView(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated, ]
    queryset = BasketProduct.objects.all()

    @swagger_auto_schema(operation_summary="Удалить из корзины")
    @action(detail=True, methods=['delete'])
    def destroy(self, request, *args, **kwargs):
        """Удаление продукта из корзины"""
        customer = CustomUser.objects.filter(phone=request.user.phone).first()
        basket = Basket.objects.filter(owner=customer, in_order=False).first()

        if customer == self.get_object().user:
            basket_product = self.get_object()
            basket.products.remove(basket_product)
            basket_product.delete()
            recalculation_basket(basket)
            return Response({'message': "Продукт был успешно удален из корзины!"},
                            status=status.HTTP_204_NO_CONTENT)
        else:
            return Response({'detail': "Not found"},
                            status=status.HTTP_404_NOT_FOUND)


class OrderView(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    ordering = ['order_date']

    def get_serializer_class(self):
        if self.action == 'patch':
            return OrderStatusChangingSerializer
        else:
            return OrderSerializer

    def get_queryset(self):
        if not self.request.user.is_anonymous:
            if self.request.user.is_staff:
                return Order.objects.all()
            customer = self.request.user
            return Order.objects.filter(customer=customer)

    @swagger_auto_schema(operation_summary="Один заказ")
    def retrieve(self, request, *args, **kwargs):
        """ # Получение конкретного заказа"""
        super(OrderView, self).retrieve(request)

    @swagger_auto_schema(operation_summary="Изменение статуса заказа", request_body=OrderStatusChangingSerializer)
    def partial_update(self, request, *args, **kwargs):
        """ # Изменение статуса заказа  """
        instance = self.get_object()
        try:
            instance.status = request.data['status']
            instance.save()
            return Response({'message': 'Статус успешно обновлен',
                             'order': OrderSerializer(instance, context=self.get_serializer_context()).data},
                            status=status.HTTP_202_ACCEPTED)
        except:
            return Response({'message': 'Неверно указанный параметр. Ожидалось "status"'}, status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(operation_summary="Удаление заказа")
    def destroy(self, request, *args, **kwargs):
        """ # Удалить конкретынй заказ"""
        instance = self.get_object()
        if instance.status == "cancelled" or instance.status == "completed":
            self.perform_destroy(instance)
            return Response(status=status.HTTP_204_NO_CONTENT)
        return Response({'message': 'Неверный статус. Ожидалось "canceled" или "completed"!'},
                        status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(operation_summary="Получение всех заказов")
    def list(self, request, *args, **kwargs):
        """ # Получение всех заказов
        ---
        Используется пагинация, максимальное количество элементов на в одном ответе 20
        """
        orders = {}
        try:
            status = request.query_params["status"]
            if status is not None:
                if not self.request.user.is_anonymous:
                    if self.request.user.is_staff:
                        orders = Order.objects.filter(status=status)
                    customer = self.request.user
                    if orders == {}:
                        orders = Order.objects.filter(customer=customer, status=status)
                    serializer = OrderSerializer(orders, many=True)
        except:
            orders = self.get_queryset()
            serializer = OrderSerializer(orders, many=True)
        return Response(serializer.data)


class OrderCreateView(viewsets.ModelViewSet):
    serializer_class = OrderCreateSerializer
    permission_classes = [IsAuthenticated, ]

    @swagger_auto_schema(operation_summary="Создание заказа")
    @action(detail=True, methods=['post'])
    def create(self, request, *args, **kwargs):
        """ # Создание нового заказа"""
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            customer = CustomUser.objects.get(phone=request.user.phone)
            basket = Basket.objects.filter(owner=customer, in_order=False).first()
            new_order = Order.objects.create(
                customer=customer,
                first_name=request.data.get('first_name'),
                last_name=request.data.get('last_name'),
                phone=request.data.get('phone'),
                address=request.data.get('address'),
                buying_type=request.data.get('buying_type'),
                comment=request.data.get('comment')
            )
            new_order.save()
            basket.in_order = True
            basket.save()
            new_order.basket = basket
            new_order.save()
            customer.orders.add(new_order)
            return Response({'message': 'Заказ успешно добавлен!',
                             'order': OrderSerializer(new_order, context=self.get_serializer_context()).data},
                            status=status.HTTP_201_CREATED)
