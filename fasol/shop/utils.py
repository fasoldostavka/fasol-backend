from django.db import models
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response


class PaginationProducts(PageNumberPagination):
    page_size = 20
    max_page_size = 1000

    def get_paginated_response(self, data):
        return Response({
            'links': {
                'next': self.get_next_link(),
                'previous': self.get_previous_link()
            },
            'total': self.page.paginator.num_pages,
            'results': data
        })


def recalculation_basket(basket):
    basket_data = basket.products.aggregate(models.Sum("total_price"), models.Sum("quantity"))
    if basket_data['total_price__sum']:
        basket.total_price = basket_data['total_price__sum']
    else:
        basket.total_price = 0
    if basket_data["quantity__sum"]:
        basket.total_products = basket_data["quantity__sum"]
    else:
        basket.total_products = 0
    basket.save()
