from rest_framework import status

from shop.tests.test_setup import TestAuthenticationSetUp


class TestAuthentication(TestAuthenticationSetUp):
    def test_user_cannot_register_with_no_data(self):
        res = self.client.post(self.register_url)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)

    def test_user_cannot_register_with_incorrect_data(self):
        res = self.client.post(self.register_url)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)

    def test_user_can_register_correctly(self):
        res = self.client.post(
            self.register_url, self.user_data, format='json'
        )
        self.assertEqual(res.data['user']['phone'], self.user_data['phone'])
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

    def test_user_cannot_get_token_with_no_data(self):
        res = self.client.post(self.token_obtain_pair_url)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)

    def test_user_cannot_get_token_with_incorrect_data(self):
        self.client.post(self.register_url, self.user_data, format='json')
        res = self.client.post(
            self.token_obtain_pair_url, {'phone': '88888888888', 'password': 'abcd1234'}, format='json')
        res_message = 'No active account found with the given credentials'
        self.assertEqual(res.data['detail'], res_message)
        self.assertEqual(res.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_user_cannot_get_token_with_correctly(self):
        self.client.post(self.register_url, self.user_data, format='json')
        res = self.client.post(
            self.token_obtain_pair_url, {'phone': '89525724670', 'password': 'qwerty1234'}, format='json')
        self.assertEqual(list(res.data), ['refresh', 'access'])
        self.assertEqual(res.status_code, status.HTTP_200_OK)
