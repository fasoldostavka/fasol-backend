from rest_framework.test import APITestCase
from django.urls import reverse


class TestAuthenticationSetUp(APITestCase):

    def setUp(self):
        self.register_url = reverse('register_user')
        self.token_obtain_pair_url = reverse('token_obtain_pair')

        self.user_data = {
            'phone': '89525724670',
            'password': 'qwerty1234'
        }

    def tearDown(self):
        return super().tearDown()
