from django.core.validators import MinLengthValidator, RegexValidator, validate_image_file_extension
from django.db import models

import users.models


class Validators:
    def min_length(length):
        return MinLengthValidator(
            limit_value=length,
            message=f"Минимальная длинна символов {length}",
        )

    phone_regex = RegexValidator(
        regex=r'^8\d{10}$',
        message="Номер телефона должен быть введен в формате: '8 xxx xxx xx xx'. Допускается до 11 цифр.",
        code='invalid'
    )
    image_validator = validate_image_file_extension


class Category(models.Model):
    name = models.CharField(max_length=255, verbose_name='Имя категории', validators=[Validators.min_length(3)])
    representation = models.ImageField(upload_to="сategory/", verbose_name='Изображение', validators=[
        Validators.image_validator
    ])
    subcategory = models.ManyToManyField('Subcategory', blank=True, null=True, related_name='related_category')

    def __str__(self):
        return self.name


class Subcategory(models.Model):
    category = models.ForeignKey(
        'Category', verbose_name='Категория',
        on_delete=models.CASCADE,
        related_name='related_subcategory'
    )
    name = models.CharField(max_length=255, verbose_name='Имя подкатегории', validators=[Validators.min_length(3)])

    def __str__(self):
        return self.name


class Product(models.Model):
    subcategory = models.ForeignKey(Subcategory, null=True, blank=True, verbose_name='Подкатегория',
                                    on_delete=models.SET_NULL)

    name = models.CharField(max_length=255, verbose_name='Наименование товара', validators=[Validators.min_length(3)])
    description = models.TextField(verbose_name='Описание', blank=True, null=True)
    representation = models.ImageField(upload_to="product/", verbose_name='Изображение', validators=[
        Validators.image_validator
    ])
    weight = models.CharField(verbose_name='Вес', max_length=10, validators=[Validators.min_length(2)])
    composition = models.TextField(verbose_name='Состав', blank=True, null=True, validators=[Validators.min_length(3)])
    price = models.DecimalField(max_digits=5, decimal_places=2, verbose_name='Цена')
    in_stock = models.BooleanField(verbose_name='В наличии', default=True)

    def __str__(self):
        return self.name


class BasketProduct(models.Model):
    user = models.ForeignKey(users.models.CustomUser, verbose_name='Покупатель', on_delete=models.CASCADE)
    basket = models.ForeignKey(
        'Basket', verbose_name='Корзина',
        on_delete=models.CASCADE,
        related_name='related_products')
    product = models.ForeignKey(Product, verbose_name='Продукт', on_delete=models.CASCADE,)

    quantity = models.PositiveIntegerField(default=1, verbose_name='Количество')
    total_price = models.DecimalField(max_digits=9, decimal_places=2, default=0, verbose_name='Итоговая стоимость')

    def __str__(self):
        return f"Продукт {self.product.name} для корзины {self.basket.id}"

    def save(self, *args, **kwargs):
        self.total_price = self.quantity * self.product.price
        super().save(*args, **kwargs)


class Basket(models.Model):
    owner = models.ForeignKey(users.models.CustomUser, null=True, verbose_name='Покупатель', on_delete=models.CASCADE)
    products = models.ManyToManyField(BasketProduct, blank=True, verbose_name='Продукты', related_name='related_basket')
    total_products = models.PositiveIntegerField(default=0, verbose_name='Всего продуктов')
    total_price = models.DecimalField(max_digits=9, decimal_places=2, default=0, verbose_name='Итоговая стоимость')
    in_order = models.BooleanField(default=False, verbose_name='В заказе')


class Order(models.Model):
    STATUS_NEW = 'new'
    STATUS_READY = 'ready'
    STATUS_COMPLETED = 'completed'
    STATUS_CANCELED = "cancelled"

    BUYING_TYPE_SELF = 'self'
    BUYING_TYPE_DELIVERY = 'delivery'
    STATUS_CHOICES = (
        (STATUS_NEW, 'Новый заказ'),
        (STATUS_READY, 'Заказ обработан'),
        (STATUS_COMPLETED, 'Заказ выполнен'),
        (STATUS_CANCELED, 'Заказ отменен')
    )

    BUYING_TYPE_CHOICES = (
        (BUYING_TYPE_SELF, 'Самовывоз'),
        (BUYING_TYPE_DELIVERY, 'Доставка')
    )

    customer = models.ForeignKey(
        users.models.CustomUser,
        verbose_name='Покупатель',
        related_name='related_orders',
        on_delete=models.CASCADE)
    basket = models.ForeignKey(Basket, verbose_name='Корзина', on_delete=models.CASCADE, null=True, blank=True)
    first_name = models.CharField(max_length=255, verbose_name='Имя', validators=[Validators.min_length(5)])
    last_name = models.CharField(max_length=255, blank=True, null=True, verbose_name='Фамилия', validators=[Validators.min_length(5)])
    phone = models.CharField(max_length=20, verbose_name='Телефон', validators=[Validators.phone_regex])
    address = models.CharField(max_length=100, verbose_name='Адрес',validators=[Validators.min_length(5)])
    status = models.CharField(
        max_length=100,
        verbose_name='Статус заказа',
        choices=STATUS_CHOICES,
        default=STATUS_NEW
    )
    buying_type = models.CharField(
        max_length=100,
        verbose_name='Тип заказа',
        choices=BUYING_TYPE_CHOICES,
        default=BUYING_TYPE_DELIVERY
    )
    comment = models.TextField(verbose_name='Коментарий к заказу', null=True, blank=True,validators=[Validators.min_length(5)])
    order_date = models.DateTimeField(auto_now=True, verbose_name='Дата заказа')
