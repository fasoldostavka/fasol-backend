# Generated by Django 3.1.7 on 2021-06-03 17:24

from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0012_alter_user_first_name_max_length'),
        ('shop', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='CustomUser',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('phone', models.CharField(max_length=12, unique=True, validators=[django.core.validators.RegexValidator(message="Phone number must be entered in the format: '8 xxx xxx xx xx'. Up to 11 digits allowed.", regex='^8\\d{10}$')], verbose_name='Мобильный телефон')),
                ('user_name', models.CharField(blank=True, max_length=150, null=True, unique=True, validators=[django.core.validators.MinLengthValidator(limit_value=5, message='Минимальная длинна 5 символов')])),
                ('first_name', models.CharField(blank=True, max_length=150, null=True, validators=[django.core.validators.MinLengthValidator(limit_value=5, message='Минимальная длинна 5 символов')], verbose_name='Имя')),
                ('last_name', models.CharField(blank=True, max_length=150, null=True, validators=[django.core.validators.MinLengthValidator(limit_value=5, message='Минимальная длинна 5 символов')], verbose_name='Фамилия')),
                ('start_date', models.DateTimeField(default=django.utils.timezone.now)),
                ('address', models.CharField(blank=True, max_length=255, null=True, validators=[django.core.validators.MinLengthValidator(limit_value=5, message='Минимальная длинна 5 символов')], verbose_name='Адрес')),
                ('is_staff', models.BooleanField(default=False)),
                ('is_active', models.BooleanField(default=True)),
                ('groups', models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.Group', verbose_name='groups')),
                ('orders', models.ManyToManyField(related_name='related_customer', to='shop.Order', verbose_name='Заказы')),
                ('user_permissions', models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.Permission', verbose_name='user permissions')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Code',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.CharField(blank=True, max_length=5)),
                ('creation_date', models.DateTimeField(default=django.utils.timezone.now)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
