import self as self
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status, generics, viewsets
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.decorators import action
from django.shortcuts import get_object_or_404
from rest_framework_simplejwt.views import TokenObtainPairView
import vonage

from .models import CustomUser, Code
from .serializers import (
    RegisterUserSerializer,
    CustomUserSerializer,
    ResetPasswordPhoneSerializer,
    ResetPasswordVerificationCodeSerializer,
    NewPasswordSerializer,
    MyTokenObtainPairSerializer,
    LogoutSerializer,
)


@swagger_auto_schema(operation_summary="Токен доступа")
class MyTokenObtainPairView(TokenObtainPairView):
    """ # Получение токена доступа"""
    serializer_class = MyTokenObtainPairSerializer



class CustomUserRegistration(generics.GenericAPIView):
    """ # Регистрация нового пользователя"""
    permission_classes = (AllowAny,)
    serializer_class = RegisterUserSerializer

    @swagger_auto_schema(operation_summary="Регистрация")
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            custom_user = serializer.save()
            if custom_user:
                return Response({"user": CustomUserSerializer(custom_user, context=self.get_serializer_context()).data,
                                 "message": "User Created Successfully.  Now perform Login to get your token",
                                 }, status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LogoutAPIView(generics.GenericAPIView):
    """ # Выход пользователя из системы"""
    serializer_class = LogoutSerializer
    permission_classes = [IsAuthenticated]

    @swagger_auto_schema(operation_summary="Выход")
    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(status=status.HTTP_204_NO_CONTENT)


class UserRetrieveUpdateView(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = CustomUserSerializer

    @swagger_auto_schema(operation_summary="Пользователь")
    @action(detail=True, methods=['get'])
    def retrieve(self, request, *args, **kwargs):
        """ # Получение данных пользователя"""
        serializer = self.serializer_class(request.user)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @swagger_auto_schema(operation_summary="Изменения пользователя")
    @action(detail=True, methods=['patch'])
    def update(self, request, *args, **kwargs):
        """ # Обновление данных пользователя"""
        serializer_data = request.data
        print(serializer_data)
        serializer = self.serializer_class(
            request.user, data=serializer_data, partial=True
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)


class ResetPasswordPhoneView(generics.GenericAPIView):
    """ # Востановление пароля"""
    permission_classes = (AllowAny,)
    serializer_class = ResetPasswordPhoneSerializer

    @swagger_auto_schema(operation_summary="Востановление пароля")
    def post(self, request, *args, **kwargs):
        if self.get_serializer(data=request.data).is_valid(raise_exception=True):
            try:
                user = CustomUser.objects.get(phone=request.data.get('phone'))
                code, create = Code.objects.get_or_create(user=user)
                code.save()
                client = vonage.Client(key="4592036e", secret="ulR8h9vHuYeGsdYV")
                sms = vonage.Sms(client)
                phone = user.phone
                phone = "7" + phone[1:]
                response = sms.send_message(
                    {
                        "from": "MyFasol",
                        "to": phone,
                        "text": f"Kod podtverzhdeniya {code.number} deystvitelen 1 minutu",
                    }
                )
                if response["messages"][0]["status"] == "0":
                    return Response({"message": "Код подтверждения отправлен на ваш телефон"},
                                    status=status.HTTP_200_OK)
                return Response({"message": "Ошибка при отправки смс. Возможно указан не верный номер"},
                                status=status.HTTP_200_OK)
            except ObjectDoesNotExist:
                return Response({"error": f"Пользователь не зарегестрирован!"},
                                status=status.HTTP_400_BAD_REQUEST)


class ResetPasswordVerificationCodeView(generics.GenericAPIView):
    """ # Отправка кода подтверждения"""
    permission_classes = (AllowAny,)
    serializer_class = ResetPasswordVerificationCodeSerializer

    @swagger_auto_schema(operation_summary="Код подтверждения")
    def post(self, request, *args, **kwargs):
        if self.get_serializer(data=request.data).is_valid(raise_exception=True):
            try:
                code = Code.objects.get(number=request.data.get('number'))
                if ((timezone.now() - code.creation_date).total_seconds()) < 60:
                    return Response({'user_id': code.user.id, 'code': code.number}, status=status.HTTP_200_OK)
                return Response({"error": f"Код подтверждения просрочен!"}, status=status.HTTP_400_BAD_REQUEST)
            except ObjectDoesNotExist:
                return Response({"error": f"Не верный код!"}, status=status.HTTP_400_BAD_REQUEST)


class SetNewPasswordView(generics.GenericAPIView):
    """ # Водд нового пороля"""
    permission_classes = (AllowAny,)
    serializer_class = NewPasswordSerializer

    @swagger_auto_schema(operation_summary="Новый пароль")
    def post(self, request, *args, **kwargs):
        if self.get_serializer(data=request.data).is_valid(raise_exception=True):
            try:
                code = Code.objects.get(number=request.data.get('code'))
                if ((timezone.now() - code.creation_date).total_seconds()) < 60:
                    user = get_object_or_404(CustomUser, id=request.data.get('user_id'))
                    if code.user != user:
                        return Response({"message": f"Не верный код!"}, status=status.HTTP_400_BAD_REQUEST)
                    password = request.data.get('password')
                    confirm_password = request.data.get('confirm_password')
                    if password == confirm_password:
                        user.set_password(password)
                        user.save()
                        code.delete()
                        return Response({"message": f"Пароль успешно изменён!"}, status=status.HTTP_200_OK)
                    return Response({"error": "Паароль подтверждения указан не верно!"},
                                    status=status.HTTP_400_BAD_REQUEST)
            except ObjectDoesNotExist:
                return Response({"error": f"Превышен лимит ожидания!"}, status=status.HTTP_400_BAD_REQUEST)
