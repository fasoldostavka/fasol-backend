from rest_framework import serializers
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.tokens import RefreshToken, TokenError
from .models import CustomUser


class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)
        token['isAdmin'] = user.is_staff

        return token


class RegisterUserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(
        max_length=128,
        min_length=6,
        write_only=True,
        label='Пароль'
    )

    class Meta:
        model = CustomUser
        fields = ('phone', 'password')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        password = validated_data.pop('password', None)
        instance = self.Meta.model(**validated_data)
        if password is not None:
            instance.set_password(password)
        instance.save()
        return instance


class LogoutSerializer(serializers.Serializer):
    refresh = serializers.CharField(label='refresh токен')

    default_error_messages = {
        'bad_token': 'Token is expired or invalid'
    }

    def validated(self, attrs):
        self.token = attrs['refresh']
        return attrs

    def save(self, **kwargs):
        try:
            RefreshToken(self.token).blacklist()
        except TokenError:
            self.fail('bad_token')


class CustomUserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(
        max_length=128,
        min_length=6,
        write_only=True
    )

    class Meta:
        model = CustomUser
        fields = ('id', 'phone', 'password', 'first_name', 'last_name', 'address')

    def update(self, instance, validated_data):
        password = validated_data.pop('password', None)
        for key, value in validated_data.items():
            setattr(instance, key, value)
        if password is not None:
            instance.set_password(password)
        instance.save()
        return instance


class ResetPasswordPhoneSerializer(serializers.Serializer):
    phone = serializers.CharField(max_length=20)


class ResetPasswordVerificationCodeSerializer(serializers.Serializer):
    number = serializers.CharField(max_length=5)


class NewPasswordSerializer(serializers.Serializer):
    password = serializers.CharField(
        max_length=128,
        min_length=8,
        write_only=True
    )
    confirm_password = serializers.CharField(
        max_length=128,
        min_length=8,
        write_only=True
    )
    code = serializers.CharField(max_length=5)
    user_id = serializers.CharField(max_length=255)
